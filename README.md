# Curriculum

The following details (as close as I could approximate) the combined Undergraduate/Graduate degree in 
Computer Science from MIT via courses available on [MIT OpenCourseware](https://ocw.mit.edu). As electives,
I have included all courses from the MIT Pure Mathematics Undergraduate degree (because I wanted to review 
the material), a sequence on Linguistics, a sequence on Mandarin Chinese as a foreign language, and a few
additional courses merely because they interested me. The additions bring the curriculum into closer 
alignment with MIT General Institute requirements by satisfying the HASS and CI portions of the 
curriculum and the Graduate requirement for exposure to a foreign language. 
There are several research based courses which are required, but not available on MIT 
OpenCourseware. To account for this, I've added additional deliverables each term in the form of 
notes/papers and additional projects.

There is a plan on coursroad for this sequence I put together to ensure
that all pre-requisites were met for this sequence: 
[This Sequence on CourseRoad](https://courseroad.mit.edu/#NjdhZ0)

## Notes / Changelog

- I swapped out mitocw Chinese for Duolingo French. I like the idea of being able to to get 
to a conversational level and practice via Netflix, and Netflix is packed with French films
- I reserve the right to jump to a later Tier at any point if I'm on a roll with something 
and want to hit the more advanced material regarding that topic sooner.

## Term 1

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.01  | [Single Variable Calculus](https://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/)  |
|  | 8.01  | [Physics I](https://ocw.mit.edu/courses/physics/8-01-physics-i-fall-2003/) |
|  | 6.0001  | [Introduction to Computer Science and Programming in Python](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/) |   |
|  | 24.900  | [Introduction to Linguistics](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-900-introduction-to-linguistics-fall-2012/) |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 2

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.02  | [Multivariable Calculus](https://ocw.mit.edu/courses/mathematics/18-02sc-multivariable-calculus-fall-2010/) |
|  | 8.02  | [Physics II](https://ocw.mit.edu/courses/physics/8-02-physics-ii-electricity-and-magnetism-spring-2007/) |
|  | 6.01 | [Introduction to Electrical Engineering and Computer Science I](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-01sc-introduction-to-electrical-engineering-and-computer-science-i-spring-2011/) |
|  | CMS.301  | [Introduction to Game Design Methods](https://ocw.mit.edu/courses/comparative-media-studies-writing/cms-301-introduction-to-game-design-methods-spring-2016/) |   |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 3

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 6.042J | [Mathematics for Computer Science](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/) |
|  | 3.091  | [Introduction to Solid State Chemistry](https://ocw.mit.edu/courses/materials-science-and-engineering/3-091sc-introduction-to-solid-state-chemistry-fall-2010/) |
|  | 6.02 | [Introduction to EECS II: Digital Communication Systems](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-02-introduction-to-eecs-ii-digital-communication-systems-fall-2012/) |
|  | 24.901  | [Language and Its Structure I: Phonology](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-901-language-and-its-structure-i-phonology-fall-2010/) | 

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 4

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.03  | [Differential Equations](https://ocw.mit.edu/courses/mathematics/18-03sc-differential-equations-fall-2011/) |
|  | 7.01  | [Fundamentals of Biology](https://ocw.mit.edu/courses/biology/7-01sc-fundamentals-of-biology-fall-2011/) |
|  | 6.004 | [Computation Structures](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-004-computation-structures-spring-2009/) |
|  | Duolingo  | French - Reach level 5 | |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 5

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.06  | [Linear Algebra](https://ocw.mit.edu/courses/mathematics/18-06sc-linear-algebra-fall-2011/) |
|  | 6.005  | [Software Construction](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-005-software-construction-spring-2016/) | 
|  | 6.006  | [Introduction to Algorithms](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/) |
|  | 24.09 | [Minds and Machines](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-09-minds-and-machines-fall-2011/) |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 6

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.100B  | [Analysis I](https://ocw.mit.edu/courses/mathematics/18-100b-analysis-i-fall-2010/)  |
|  | 6.170  | [Software Studio](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-170-software-studio-spring-2013/)  |
|  | 6.046J  | [Design and Analysis of Algorithms](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/) |
|  | 24.902  | [Language and its Structure II: Syntax](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-902-language-and-its-structure-ii-syntax-fall-2015/) |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 7

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.103B  | [Fourier Analysis](https://ocw.mit.edu/courses/mathematics/18-103-fourier-analysis-fall-2013/)  |
|  | 6.172  | [Performance Engineering of Software Systems](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-172-performance-engineering-of-software-systems-fall-2010/index.htm) |
|  | 6.034  | [Artificial Intelligence](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/) |
|  | Duolingo  | French - Reach level 10 | |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 8

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.701  | [Algebra I](https://ocw.mit.edu/courses/mathematics/18-701-algebra-i-fall-2010/)  |
|  | 6.033  | [Computer System Engineering](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-033-computer-system-engineering-spring-2009/) |
|  | 6.821  | [Programming Languages](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-821-programming-languages-fall-2002/)  |
|  | 24.903  | [Language and its Structure III: Semantics and Pragmatics](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-903-language-and-its-structure-iii-semantics-and-pragmatics-spring-2005/) |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 9

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.702  | [Algebra II](https://ocw.mit.edu/courses/mathematics/18-702-algebra-ii-spring-2011/)  |
|  | 6.045J  | [Automata, Computability, and Complexity](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-045j-automata-computability-and-complexity-spring-2011/) |
|  | 6.854 | [Advanced Algorithms](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-854j-advanced-algorithms-fall-2008/) |
|  | Duolingo  | French - Reach level 15 | |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 10

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 18.901  | [Introduction to Topology](https://ocw.mit.edu/courses/mathematics/18-901-introduction-to-topology-fall-2004/)  |
|  | 6.828  | [Operating System Engineering](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-828-operating-system-engineering-fall-2012/) |
|  | 6.852  | [Distributed Algorithms](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-852j-distributed-algorithms-fall-2009/)  |
|  | CMS.611 | [Creating Video Games](https://ocw.mit.edu/courses/comparative-media-studies-writing/cms-611j-creating-video-games-fall-2014/) |   |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 11

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 6.851  | [Advanced Data Structures](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-851-advanced-data-structures-spring-2012/) |
|  | 6.875  | [Cryptography and Cryptanalysis](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-875-cryptography-and-cryptanalysis-spring-2005/) |
|  | Duolingo  | French - Reach level 20 | |
|  | 8.225  | [Einstein, Oppenheimer, Feynman: Physics in the 20th Century](https://ocw.mit.edu/courses/science-technology-and-society/sts-042j-einstein-oppenheimer-feynman-physics-in-the-20th-century-spring-2011/)  |


### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---
## Term 12

| Progress | Course Number   | Course Name  | Notes/Assignments |
|---|---|---|---|---|
|  | 6.856 | [Randomized Algorithms](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-856j-randomized-algorithms-fall-2002/) |
|  | 6.876J  | [Advanced Topics in Cryptography](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-876j-advanced-topics-in-cryptography-spring-2003/) | 
|  | Duolingo  | French - Reach level 25 | |
|  | 21.221 | [Metaphysics](https://ocw.mit.edu/courses/linguistics-and-philosophy/24-221-metaphysics-spring-2015/index.htm?utm_source=OCWDept&utm_medium=CarouselSm&utm_campaign=FeaturedCourse) |

### Deliverables
- [ ] Paper or Blog Post illustrating what was learned this term
- [ ] Project which illustrates what was learned this term 
- [ ] Course Notes for all courses this term 
- [ ] Completed exams for this term 

---